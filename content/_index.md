---
title: "online bill pay"
date: 2021-01-19T18:27:04-05:00
draft: false
---

Please enter our `Account Number` found on your statement where circled below into the top most `PayPal` field titled
`Description` and the amount you would like to pay in the `Price per item` field after clicking `Pay Now`, thank you.

{{< rawhtml >}}

<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
<input type="hidden" name="cmd" value="_s-xclick">
<input type="hidden" name="hosted_button_id" value="MH6RRSR46Q5VC">
<input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_paynowCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
<img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
</form>


{{< /rawhtml >}}

![Etactics image](/etactics-example.png)

prior statements

![prior image](/rri-example.png)

